package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCH_FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String COMMODITY_FILE_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String BRANCH_FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String COMMODITY_FILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String OVER_10DIGIT = "合計金額が10桁を超えました";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイルが連番になっていません";
	private static final String FILE_INVALID_BRANCHCODE = "の支店コードが不正です";
	private static final String SALESFILE_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//エラー処理3-1
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 正規表現の変数
		String branchCode = "^[0-9]{3}$";
		String commodityCode = "^[A-Za-z0-9]{8}$";

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchCode, BRANCH_FILE_NOT_EXIST, BRANCH_FILE_INVALID_FORMAT)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityCode, COMMODITY_FILE_NOT_EXIST, COMMODITY_FILE_INVALID_FORMAT)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {

			//ファイルであるか、ファイル名のチェック
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//ファイル並び替え
		Collections.sort(rcdFiles);

		//エラー処理2-1
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			String rcdName = rcdFiles.get(i).getName();
			String rcdName1 = rcdFiles.get(i + 1).getName();

			int former = Integer.parseInt(rcdName.substring(0, 8));
			int latter = Integer.parseInt(rcdName1.substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		BufferedReader br = null;

		for (int i = 0; i < rcdFiles.size(); i++) {
			//リストをファイルへ
			File saleFile = rcdFiles.get(i);

			try {
				//ファイル読み込み
				FileReader fr= new FileReader(saleFile);
				br = new BufferedReader(fr);

				//ファイルをリストへ
				String saleLine;
				List<String> salesList = new ArrayList<>();
				while ((saleLine = br.readLine()) != null) {
					salesList.add(saleLine);
				}

				//エラー処理2-5
				if (salesList.size() != 3) {
					System.out.println(rcdFiles.get(i) + SALESFILE_INVALID_FORMAT);
					return;
				}

				//エラー処理2-3
				if (!branchNames.containsKey(salesList.get(0))) {
					System.out.println(rcdFiles.get(i) + FILE_INVALID_BRANCHCODE);
					return;
				}

				//エラー処理2-4
				if (!commodityNames.containsKey(salesList.get(1))) {
					System.out.println(rcdFiles.get(i) + FILE_INVALID_BRANCHCODE);
					return;
				}

				//エラー処理3-2
				if (!salesList.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//Long型にキャスト
				Long fileSales = Long.parseLong(salesList.get(2));

				//金額を加算
				Long branchSalesAmount = branchSales.get(salesList.get(0)) + fileSales;
				Long commoditySalesAmount = commoditySales.get(salesList.get(1)) + fileSales;

				//エラー処理2-2(支店別)
				if (branchSalesAmount >= 10000000000L) {
					System.out.println(OVER_10DIGIT);
					return;
				}

				//エラー処理2-2(商品別)
				if (commoditySalesAmount >= 10000000000L) {
					System.out.println(OVER_10DIGIT);
					return;
				}

				//mapへ格納
				branchSales.put(salesList.get(0), branchSalesAmount);
				commoditySales.put(salesList.get(1), commoditySalesAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String code, String notFile, String fileInvalidFormat) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//エラー処理1-1,1-3
			if (!file.exists()) {
				System.out.println(notFile);
				return false;
			}

			//ファイル読み込み
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//エラー処理1-2,1-4
				if ((items.length != 2) || (!items[0].matches(code))) {
					System.out.println(fileInvalidFormat);
					return false;
				}

				//mapへ格納
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchCommodityNames, Map<String, Long> branchCommoditySales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File outPutfile = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			//ファイルへ書き込み
			FileWriter fw = new FileWriter(outPutfile);
			bw = new BufferedWriter(fw);
			for (String key : branchCommodityNames.keySet()) {
				String names = branchCommodityNames.get(key);
				Long salesNum = branchCommoditySales.get(key);

				bw.write(key + "," + names + "," + salesNum.toString());
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
